import '@babel/polyfill'
import Vue from 'vue'
import VueRouter from 'vue-router'
import './plugins/vuetify.js'

import App from './App.vue'
import Auth from './views/Auth'
import Reg from './views/Reg'
import Recover from './views/Recover'
import Dashboard from './views/Dashboard'
import DocumentList from './views/DocumentList'
import DocumentDetail from './views/DocumentDetail'
import CertificateList from './views/CertificateList'
import OrganizationList from './views/OrganizationList'
import DocumentSharedDetail from './views/DocumentSharedDetail'
import DocumentShareUpload from './views/DocumentShareUpload'
import store from './store/index.js'

import '@mdi/font/css/materialdesignicons.css'
import './assets/css/main.css'
import moment from 'moment'

Vue.config.productionTip = false

Vue.use(VueRouter)

Vue.filter('date', function (date) {
  return moment(date).format('DD.MM.YY HH:mm:ss')
})

const routes = [
  {
    path: '/', component: Dashboard, redirect: '/documents/pending',
    children: [
      {
        path: 'documents/all',
        component: DocumentList,
        name: 'all_document_list',
        props: {
          mode: 'all',
          title: 'Все документы',
          inbox_name: 'Контрагент (ЕДРПОУ/ДРФО)'
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/all/:document_id',
        component: DocumentDetail,
        name: 'all_document_detail',
        props: {
          mode: 'all',
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/pending',
        component: DocumentList,
        name: 'pending_document_list',
        props: {
          mode: 'pending',
          title: 'Загруженные документы',
          inbox_name: 'Контрагент (ЕДРПОУ/ДРФО)'
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/pending/:document_id',
        component: DocumentDetail,
        name: 'pending_document_detail',
        props: {
          mode: 'pending',
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/sent',
        component: DocumentList,
        name: 'sent_document_list',
        props: {
          mode: 'sent',
          title: 'Отправленные документы',
          inbox_name: 'Получатель (ЕДРПОУ/ДРФО)'
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/sent/:document_id',
        component: DocumentDetail,
        name: 'sent_document_detail',
        props: {
          mode: 'pending',
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/inbox',
        component: DocumentList,
        name: 'inbox_document_list',
        props: {
          mode: 'inbox',
          title: 'Входящие документы',
          inbox_name: 'Отправитель (ЕДРПОУ/ДРФО)'
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/inbox/:document_id',
        component: DocumentDetail,
        name: 'inbox_document_detail',
        props: {
          mode: 'inbox',
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/archive',
        component: DocumentList,
        name: 'archive_document_list',
        props: {
          mode: 'archive',
          title: 'Архивные документы',
          inbox_name: 'Контрагент (ЕДРПОУ/ДРФО)'
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'documents/archive/:document_id',
        component: DocumentDetail,
        name: 'archive_document_detail',
        props: {
          mode: 'archive',
        },
        meta: { requiresAuth: true }
      },
      {
        path: 'certificates',
        component: CertificateList,
        name: 'certificate_list',
        meta: { requiresAuth: true }
      },
      {
        path: 'organizations',
        component: OrganizationList,
        name: 'organization_list',
        meta: { requiresAuth: true }
      }
    ]
  },
  { path: '/auth', component: Auth, name: 'auth' },
  { path: '/reg', component: Reg, name: 'reg' },
  { path: '/recover', component: Recover, name: 'recover' },
  { path: '/shared/:document_id', component: DocumentSharedDetail, name: 'shared' },
  { path: '/share', component: DocumentShareUpload }
]

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

new Vue({
  store,
  router,
  created () {
    let self = this
    let eusign = new EUSign() // eslint-disable-line no-undef
    eusign.initialize(
    function () {
        self.$store.commit('setEUSign', eusign)
        self.$store.commit('setCas', eusign.m_cas)
        console.log('Success initalized EUSign') // eslint-disable-line no-console
    }, 
    function () {
        console.log('Failed initalized EUSign') // eslint-disable-line no-console
    })
  },
  render: h => h(App)
}).$mount('#app')
