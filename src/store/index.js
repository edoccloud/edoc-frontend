import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'


axios.defaults.baseURL = process.env.VUE_APP_API_URL // eslint-disable-line no-undef


Vue.use(VueAxios, axios)
Vue.use(Vuex)


Vue.axios.interceptors.request.use(
  config => {
    let token = localStorage.getItem('token')
    if (token) {
      config.headers.Authorization = `Token ${token}`
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

Vue.axios.interceptors.response.use(
  response => {
    return Promise.resolve(response)
  },
  error => {
    if (error.response.status == 401) {
      localStorage.clear()
      location.href = '/auth'
    }
    return Promise.reject(error)
  }
)


export default new Vuex.Store({
  state: {
    eusign: null,
    cas: [],
    documents: [],
    profile: {},
    documents_next: null,
    certificates: [],
    applications: [],
    organizations: []
  },
  mutations: {
    setEUSign(state, eusign) {
      state.eusign = eusign
    },
    setCas(state, cas) {
      state.cas = cas
    },
    setApplications(state, applications) {
      state.applications = applications
    },
    setDocuments(state, documents) {
      state.documents = documents
    },
    setOrganizations(state, organizations) {
      state.organizations = organizations
    },
    setDocumentsNext(state, next_link) {
      state.documents_next = next_link
    },
    setCertificates(state, certificates) {
      state.certificates = certificates
    },
    setProfile(state, profile) {
      state.profile = profile
    }
  },
  actions: {
    userLogin({ commit }, data) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/auth/', data)
    },
    userReg({ commit }, data) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/reg/', data)
    },
    userRegConfirm({ commit }, data) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/reg/confirm/', data)
    },
    userSendRecoverSms({ commit }, data) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/recover/sms/', data)
    },
    userChangePassword({ commit }, data) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/recover/password/', data)
    },
    getProfile({ commit }, data) {
      return Vue.axios.get('/v1/profile/', data).
        then((response) => {
          commit('setProfile', response.data)
        })
    },
    getDocumentById({ commit }, document_id) { // eslint-disable-line no-unused-vars
      return Vue.axios.get(`/v1/documents/${document_id}`)
    },
    getApplicationList({ commit }) {
      return Vue.axios.get('/v1/applications/')
        .then((response) => {
          commit('setApplications', response.data)
        })
    },
    getDocumentList({ commit }, data) {
      let mode = data['mode']
      let params = {}
      if (data['search'] !== '') {
        Object.assign(params, {search: data['search']})
      }
      if (data['start_date'] !== null) {
        Object.assign(params, {start_date: data['start_date']})
      }
      if (data['end_date'] !== null) {
        Object.assign(params, {end_date: data['end_date']})
      }
      return Vue.axios.get(`/v1/documents/${mode}`, {params})
        .then((response) => {
          commit('setDocuments', response.data.data)
          commit('setDocumentsNext', response.data.next)
        })
    },
    nextDocumentList({ commit, state }) {
      let parser = document.createElement('a');
      parser.href = state.documents_next;
      let url = [parser.pathname, parser.search].join('')
      return Vue.axios.get(url)
        .then((response) => {
          let old_list = state.documents
          commit('setDocuments', old_list.concat(response.data.data))
          commit('setDocumentsNext', response.data.next)
        })
    },
    getOrganizationList({ commit }) {
      return Vue.axios.get('/v1/organizations/')
        .then((response) => {
          commit('setOrganizations', response.data.data)
        })
    },
    postOrganization({ commit }, data) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/organizations/', data)
    },
    deleteOrganization({ commit }, organization_id) { // eslint-disable-line no-unused-vars
      return Vue.axios.delete(`/v1/organizations/${organization_id}`)
    },
    postDocument({ commit }, fd) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/documents/', fd)
    },
    postSign({ commit }, {document_id, fd}) { // eslint-disable-line no-unused-vars
      return Vue.axios.post(`/v1/documents/${document_id}/sign`, fd)
    },
    sendDocument({ commit }, {document_id, data}) { // eslint-disable-line no-unused-vars
      return Vue.axios.put(`/v1/documents/${document_id}/send`, data)
    },
    archiveDocument({ commit }, document_id) { // eslint-disable-line no-unused-vars
      return Vue.axios.put(`/v1/documents/${document_id}/archive`)
    },
    deleteDocument({ commit }, document_id) { // eslint-disable-line no-unused-vars
      return Vue.axios.delete(`/v1/documents/${document_id}`)
    },
    getSharedDocument({ commit }, share_code) { // eslint-disable-line no-unused-vars
      return Vue.axios.get(`/v1/documents/${share_code}`)
    },
    shareDocument({ commit }, document_id) { // eslint-disable-line no-unused-vars
      return Vue.axios.put(`/v1/documents/${document_id}/share`)
    },
    unshareDocument({ commit }, document_id) { // eslint-disable-line no-unused-vars
      return Vue.axios.put(`/v1/documents/${document_id}/unshare`)
    },
    getCertificateList({ commit }) {
      return Vue.axios.get('/v1/certificates/')
        .then((response) => {
          commit('setCertificates', response.data.data)
        })
    },
    addCertificate({ commit }, fd) { // eslint-disable-line no-unused-vars
      return Vue.axios.post('/v1/certificates/', fd)
    },
    getCertificate({ commit }, certificate_serial) { // eslint-disable-line no-unused-vars
      return Vue.axios.get(`/v1/certificates/${certificate_serial}`)
    },
    delCertificate({ commit }, certificate_serial) { // eslint-disable-line no-unused-vars
      return Vue.axios.delete(`/v1/certificates/${certificate_serial}`)
    }
  }
})