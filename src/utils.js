function readFileFromComputer(file) {
    let reader = new FileReader()
    return new Promise((resolve, reject) => {
        reader.onloadend = (event) => {
            resolve(event.target.result)
        }
        reader.onerror = (event) => {
            reject(event)
        }
        reader.readAsArrayBuffer(file)
    })
}

function readPrivateKey(uint8_data, password, issuer, eusign) {
    return new Promise((resolve, reject) => { // eslint-disable-line no-unused-vars

      function onSuccess(certificate) {
        resolve(certificate)
      }

      function onError(err) {
        resolve(err)
      }

      eusign.readPrivateKeyBinary(
        uint8_data,
        password,
        [],
        issuer,
        onSuccess,
        onError
      )
    })
}

function signFile(uint8_data, sign_algo, external, append_cert, eusign) {

    return new Promise((resolve, reject) => { // eslint-disable-line no-unused-vars

        function on_success(certificate, sign) {
          resolve({sign_info: certificate, sign: sign})
        }
  
        function on_error(err) {
          resolve(err)
        }

        eusign.signFile(
          uint8_data,
          sign_algo,
          external,
          append_cert,
          on_success,
          on_error
        )
    })
}

function enumJKSPrivateKeys(container, eusign) {

    return new Promise((resolve, reject) => { // eslint-disable-line no-unused-vars

        function on_success(aliases) {
          resolve(aliases)
        }

        function on_error(err) {
          resolve(err)
        }

        eusign.enumJKSPrivateKeys(container, 
          on_success, 
          on_error)
    })

}

function getJKSPrivateKey(container, alias, eusign) {

    return new Promise((resolve, reject) => { // eslint-disable-line no-unused-vars

        function on_success(jksPrivateKey) {
          resolve(jksPrivateKey)
        }

        function on_error(err) {
          resolve(err)
        }

        eusign.getJKSPrivateKey(
          container, 
          alias,
          on_success, 
          on_error)
    })

}

function parseCertificate(certificate, eusign) {

  return new Promise((resolve, reject) => { // eslint-disable-line no-unused-vars

      function on_success(cert) {
        resolve(cert)
      }

      function on_error(err) {
        resolve(err)
      }

      eusign.parseCertificate(
        certificate,
        on_success, 
        on_error)
  })

}

export { readFileFromComputer, readPrivateKey, signFile, enumJKSPrivateKeys, getJKSPrivateKey, parseCertificate }
